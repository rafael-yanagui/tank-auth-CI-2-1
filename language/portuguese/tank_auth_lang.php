<?php

// Errors
$lang['auth_incorrect_password'] = 'Senha incorreta';
$lang['auth_incorrect_login'] = 'Login incorreto';
$lang['auth_incorrect_email_or_username'] = 'Login ou e-mail não existe';
$lang['auth_email_in_use'] = 'Esse e-mail já esta cadastrado por outro usuário. Por favor escolha outro e-mail.';
$lang['auth_username_in_use'] = 'CPF já foi cadastrado. Por favor verifique se você digitou corretamente ou tente redefinir sua senha.';
$lang['auth_current_email'] = 'Esse é seu atual e-mail';
$lang['auth_incorrect_captcha'] = 'Seu código de confirmação não corresponde com o da imagem.';
$lang['auth_captcha_expired'] = 'Seu código de confirmação está expirado. Por favor tente novamente.';

// Notifications
$lang['auth_message_logged_out'] = 'Você saiu com sucesso.';
$lang['auth_message_registration_disabled'] = 'Registro esta desabilitado.';
$lang['auth_message_registration_completed_1'] = 'Você efetuou seu cadastro com sucesso. Verifique seu e-mail para ativar sua conta.';
$lang['auth_message_registration_completed_2'] = 'Você foi registrado com sucesso.';
$lang['auth_message_activation_email_sent'] = 'Um novo e-mail de ativação foi enviado para %s. Siga as instruções dentro do corpo do e-mail e ative sua conta.';
$lang['auth_message_activation_completed'] = 'Sua conta foi ativada com sucesso.';
$lang['auth_message_activation_failed'] = 'O código de ativação inserido está incorreto ou expirado.';
$lang['auth_message_password_changed'] = 'Sua senha foi alterada com sucesso.';
$lang['auth_message_new_password_sent'] = 'Um e-mail com as instruções de alterações de senha foi criado e enviado para você.';
$lang['auth_message_new_password_activated'] = 'Sua senha foi alterada, faça login utilizando sua nova senha.';
$lang['auth_message_new_password_failed'] = 'Sua chave de ativação está incorreta ou expirada. Por favor verifique seu e-mail novamente e siga as instruções.';
$lang['auth_message_new_email_sent'] = 'Uma mensagem de confirmação foi enviada para o e-mail %s. Siga as instruções no e-mail para alterar completar a trocar de endereços de e-mail.';
$lang['auth_message_new_email_activated'] = 'Você alterou seu e-mail com sucesso';
$lang['auth_message_new_email_failed'] = 'Sua chave de ativação está incorreta ou expirada. Por favor verifique seu e-mail novamente e siga as instruções.';
$lang['auth_message_banned'] = 'Você está proibido.';
$lang['auth_message_unregistered'] = 'Sua conta foi deletada...';

// Email subjects
$lang['auth_subject_welcome'] = 'Bem Vindo ao %s!';
$lang['auth_subject_activate'] = 'Bem Vindo ao %s!';
$lang['auth_subject_forgot_password'] = 'Esqueceu sua senha em %s?';
$lang['auth_subject_reset_password'] = 'Sua nova senha em %s';
$lang['auth_subject_change_email'] = 'Seu novo endereço de e-mail em %s';
