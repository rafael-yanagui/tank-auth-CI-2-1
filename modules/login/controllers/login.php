<?php
/**
 * Created by PhpStorm.
 * User: maurilio
 * Date: 26/07/16
 * Time: 10:40
 */

if (!defined('BASEPATH')) {
    exit('Acesso negado!');
}

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');


    }

    public function index()
    {

        if ($this->tank_auth->is_logged_in()) {                                    // logged in
            redirect('');
        } elseif ($this->tank_auth->is_logged_in(FALSE)) {                        // logged in, not activated
            redirect(base_url('/login/reenviar_confirmacao/'));
        } else {

            $data['login_by_username'] = ($this->config->item('login_by_username', 'tank_auth') AND $this->config->item('use_username', 'tank_auth'));
            $data['login_by_email'] = $this->config->item('login_by_email', 'tank_auth');

            $this->form_validation->set_rules('login', 'Login', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'Senha', 'trim|required|xss_clean');
            $this->form_validation->set_rules('remember', 'Remember me', 'integer');

            // Get login for counting attempts to login
            if ($this->config->item('login_count_attempts', 'tank_auth') AND
                ($login = $this->input->post('login'))
            ) {
                $login = $this->security->xss_clean($login);
            } else {
                $login = '';
            }

            $data['use_recaptcha'] = $this->config->item('use_recaptcha', 'tank_auth');
            if ($this->tank_auth->is_max_login_attempts_exceeded($login)) {
                if ($data['use_recaptcha'])
                    $this->form_validation->set_rules('g-recaptcha-response', 'Recaptcha', 'trim|xss_clean|required|callback__check_recaptcha');
                else
                    $this->form_validation->set_rules('captcha', 'Confirmation Code', 'trim|xss_clean|required|callback__check_captcha');
            }
            $data['errors'] = array();

            if ($this->form_validation->run()) {                                // validation ok
                if ($this->tank_auth->login(
                    $this->form_validation->set_value('login'),
                    $this->form_validation->set_value('password'),
                    $this->form_validation->set_value('remember'),
                    $data['login_by_username'],
                    $data['login_by_email'])
                ) {                                // success
                    redirect('');
                } else {
                    $errors = $this->tank_auth->get_error_message();
                    if (isset($errors['banned'])) {                                // banned user
                        $this->_show_message($this->lang->line('auth_message_banned') . ' ' . $errors['banned']);

                    } elseif (isset($errors['not_activated'])) {                // not activated user
                        redirect(base_url('/login/reenviar_confirmacao/'));

                    } else {

                        foreach ($errors as $k => $v) {
                            $this->form_validation->set_error($k, $this->lang->line($v));
                        }
                    }
                }
            }

            $data['show_captcha'] = FALSE;

            if ($this->tank_auth->is_max_login_attempts_exceeded($login)) {
                $data['show_captcha'] = TRUE;
                $data['recaptcha'] = $this->_create_recaptcha();
            }
        }


        $data['title'] = "Acesse o Buscador Dizer o Direito";
        $data['description'] = "Faça login e tenha acessa a julgados comentados de todas as áreas do Direito";
        $data['menu_ativo'] = "login";

        $this->template->show('login/view_default', $data);

    }

    public function cadastro()
    {
        if ($this->tank_auth->is_logged_in()) {                                    // Se estiver Logado
            redirect('');

        } elseif ($this->tank_auth->is_logged_in(FALSE)) {                        // Está logado mas não ativou a conta
            redirect(base_url('/login/reenviar_confirmacao/'));

        } elseif (!$this->config->item('allow_registration', 'tank_auth')) {    // Registro bloqueado
            $this->_show_message($this->lang->line('auth_message_registration_disabled'));

        } else {

            // Valido os campos do formulário
            $use_username = $this->config->item('use_username', 'tank_auth');
            if ($use_username) {
                $this->form_validation->set_rules('username', 'CPF', 'trim|required|xss_clean|min_length[' . $this->config->item('username_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('username_max_length', 'tank_auth') . ']');
            }

            $this->form_validation->set_rules('firstname', 'Nome', 'trim|required|xss_clean|min_length[' . $this->config->item('firstname_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('firstname_max_length', 'tank_auth') . ']');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
            $this->form_validation->set_rules('password', 'Senha', 'trim|required|xss_clean|min_length[' . $this->config->item('password_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth') . ']');
            $this->form_validation->set_rules('confirm_password', 'Repetir senha', 'trim|required|xss_clean|matches[password]');

            // Se estiver utilizando o recaptcha, adiciono nas regras do form validation
            $recaptcha_registration = $this->config->item('recaptcha_registration', 'tank_auth');
            if ($recaptcha_registration) {
                $this->form_validation->set_rules('g-recaptcha-response', 'Recaptcha', 'trim|xss_clean|required|callback__check_recaptcha');
            }
            $data['errors'] = array();

            if ($this->form_validation->run()) { // validation ok

                //Faço o tratamento do nome para cadastrar nome separado de sobrenome

                $name = explode(' ', $this->form_validation->set_value('firstname'));

                $firstName = $name[0];
                unset($name[0]);
                $lastName = implode(' ', $name);

                // Verifico se a configuração obriga a ativação da conta por e-mail
                $email_activation = $this->config->item('email_activation', 'tank_auth');
                $require_complete_profile = $this->config->item('require_complete_profile', 'tank_auth');

                // Cadastro o usuário
                if (!is_null($data = $this->tank_auth->create_user(
                    $use_username ? $this->form_validation->set_value('username') : '',
                    $firstName,
                    $lastName,
                    $this->form_validation->set_value('email'),
                    $this->form_validation->set_value('password'),
                    $email_activation,
                    $require_complete_profile))
                ) {// success

                    $data['site_name'] = $this->config->item('website_name', 'tank_auth');


                    // SE a confirmação do e-mail é obrigatório envio o e-mail de ativação
                    if ($email_activation) {
                        $data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;

                        $this->_send_email('activate', $data['email'], $data);

                        unset($data['password']); // Clear password (just for any case)

                        $this->_show_message($this->lang->line('auth_message_registration_completed_1'));

                    } else { // SE a confirmação do e-mail não for obrigatório

                        // Envio um e-mail de Seja Bem Vindo
                        if ($this->config->item('email_account_details', 'tank_auth')) {
                            $this->_send_email('welcome', $data['email'], $data);
                        }


                        // Faço login e envio usuário para preecher os os dados de perfil
                        $data['login_by_username'] = ($this->config->item('login_by_username', 'tank_auth') AND $this->config->item('use_username', 'tank_auth'));
                        $data['login_by_email'] = $this->config->item('login_by_email', 'tank_auth');

                        if ($this->tank_auth->login(
                            $use_username == TRUE ? $data['username'] : $data['email'],
                            $data['password'],
                            FALSE,
                            $data['login_by_username'],
                            $data['login_by_email'])
                        ) {                                // success
                            redirect(base_url());
                        } else {
                            $this->_show_message($this->lang->line('auth_message_registration_completed_2'));
                        }
                        unset($data['password']); // Clear password (just for any case)

                    }
                } else {

                    $errors = $this->tank_auth->get_error_message();
                    foreach ($errors as $k => $v) {
                        $this->form_validation->set_error($k, $this->lang->line($v));
                    }
                }
            }


            if ($recaptcha_registration) { // Se recaptcha for obrigatório, gero o HTML dele
                $data['recaptcha_html'] = $this->_create_recaptcha();
            }

            $data['use_username'] = $use_username;
            $data['recaptcha_registration'] = $recaptcha_registration;


            $data['title'] = "Faça parte do Buscador Dizer o Direito";
            $data['description'] = "Crie sua conta e encontre julgados comentados de todas as áreas do Direito";
            $data['menu_ativo'] = "cadastro";


            $this->template->show('view_cadastro', $data);
        }
    }

    public function reenviar_confirmacao()
    {
        if (!$this->tank_auth->is_logged_in(FALSE)) {                            // not logged in or activated
            redirect(base_url('/login/'));

        } else {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');

            $data['errors'] = array();

            if ($this->form_validation->run()) {
                // validation ok
                if (!is_null($data = $this->tank_auth->change_email(
                    $this->form_validation->set_value('email')))
                ) {            // success

                    $data['site_name'] = $this->config->item('website_name', 'tank_auth');
                    $data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;

                    $this->_send_email('activate', $data['email'], $data);

                    $this->_show_message(sprintf($this->lang->line('auth_message_activation_email_sent'), $data['email']));

                } else {
                    $errors = $this->tank_auth->get_error_message();
                    foreach ($errors as $k => $v) {
                        $this->form_validation->set_error($k, $this->lang->line($v));
                    }
                }
            }


            $data['title'] = "Ative sua conta!";
            $data['description'] = "Antes de fazer login, você precisa ativar a sua conta.";
            $data['menu_ativo'] = "cadastro";


            $this->template->show('view_reenviar_confirmacao', $data);
        }

    }

    function ativacao($user_id, $new_email_key)
    {

        // Activate user
        if ($this->tank_auth->activate_user($user_id, $new_email_key)) {        // success

            $this->_show_message($this->lang->line('auth_message_activation_completed', 'tank-auth'));
        } else {                                                                // fail
            $this->_show_message($this->lang->line('auth_message_activation_failed', 'tank_auth'), 'danger');
        }
    }

    function esqueci_a_senha()
    {
        if ($this->tank_auth->is_logged_in()) {                                    // logged in
            redirect('');


        } elseif ($this->tank_auth->is_logged_in(FALSE)) {                        // logged in, not activated
            redirect(base_url('/login/reenviar_confirmacao/'));

        } else {

            $this->form_validation->set_rules('login', 'Email or login', 'trim|required|xss_clean');

            $data['errors'] = array();

            if ($this->form_validation->run()) { // validation ok

                if (!is_null($data = $this->tank_auth->forgot_password(
                    $this->form_validation->set_value('login')))
                ) {


                    $data['site_name'] = $this->config->item('website_name', 'tank_auth');

                    // Send email with password activation link
                    $this->_send_email('forgot_password', $data['email'], $data);

                    $this->_show_message($this->lang->line('auth_message_new_password_sent'));

                } else {
                    $errors = $this->tank_auth->get_error_message();
                    foreach ($errors as $k => $v)
                        $this->form_validation->set_error($k, $this->lang->line($v));
                }
            }


            $data['title'] = "Esqueci a senha!";
            $data['description'] = "Informe seu e-mail no campo abaixo para redefinir a senha.";
            $data['menu_ativo'] = "cadastro";

            $this->template->show('view_esqueci_senha', $data);
        }
    }

    function alterar_senha($user_id, $new_pass_key)
    {

        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length[' . $this->config->item('password_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth') . ']|alpha_dash');
        $this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

        $data['errors'] = array();

        if ($this->form_validation->run()) {                                // validation ok
            if (!is_null($data = $this->tank_auth->reset_password(
                $user_id, $new_pass_key,
                $this->form_validation->set_value('new_password')))
            ) {    // success

                $data['site_name'] = $this->config->item('website_name', 'tank_auth');

                // Send email with new password
                $this->_send_email('reset_password', $data['email'], $data);

                $this->_show_message($this->lang->line('auth_message_new_password_activated'), 'success');

            } else {                                                        // fail
                $this->_show_message($this->lang->line('auth_message_new_password_failed'), 'danger');
            }
        } else {
            // Try to activate user by password key (if not activated yet)
            if ($this->config->item('email_activation', 'tank_auth')) {
                $this->tank_auth->activate_user($user_id, $new_pass_key, FALSE);
            }

            if (!$this->tank_auth->can_reset_password($user_id, $new_pass_key)) {
                $this->_show_message($this->lang->line('auth_message_new_password_failed'), 'danger');
            }
        }
        $data['title'] = "Alterar Senha!";
        $data['description'] = "Digite sua nova senha nos campos abaixo.";
        $data['menu_ativo'] = "cadastro";

        $this->template->show('view_alterar_senha', $data);
    }


    function logout()
    {
        $this->tank_auth->logout();
        $this->_show_message($this->lang->line('auth_message_logged_out', 'tank_auth'));
    }


    /**
     * Delete user from the site (only when user is logged in)
     *
     * @return void
     */
    //TODO TESTAR DELETAR CONTA
//    function deletar_conta()
//    {
//        if (!$this->tank_auth->is_logged_in()) {                                // not logged in or not activated
//            redirect(base_url('/login/'));
//
//        } else {
//            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
//
//            $data['errors'] = array();
//
//            if ($this->form_validation->run()) {                                // validation ok
//                if ($this->tank_auth->delete_user(
//                    $this->form_validation->set_value('password'))
//                ) {        // success
//                    $this->_show_message($this->lang->line('auth_message_unregistered'));
//
//                } else {                                                        // fail
//                    $errors = $this->tank_auth->get_error_message();
//                    foreach ($errors as $k => $v) $data['errors'][$k] = $this->lang->line($v);
//                }
//            }
//            $this->load->view('view_deletar_conta', $data);
//        }
//    }


    /**
     * Show info message
     *
     * @param    string
     * @return    void
     */
    function _show_message($message, $type = 'default')
    {
        if ($type == 'danger')
            $this->session->set_flashdata('alert_controller_danger', $message);
        else if ($type == 'warning')
            $this->session->set_flashdata('alert_controller_warning', $message);
        else if ($type == 'success')
            $this->session->set_flashdata('alert_controller_success', $message);
        else
            $this->session->set_flashdata('msg_controller', $message);

        if ($this->tank_auth->is_logged_in()) {                                    // logged in
            redirect('');
        } elseif ($this->tank_auth->is_logged_in(FALSE)) {                        // logged in, not activated
            redirect(base_url('/login/reenviar_confirmacao/'));
        } else {
            redirect(base_url('/login/'));
        }
    }

    /**
     * Send email message of given type (activate, forgot_password, etc.)
     *
     * @param    string
     * @param    string
     * @param    array
     * @return    void
     */
    function _send_email($type, $email, &$data)
    {
        $this->load->library('email');
        $this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
        $this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
        $this->email->to($email);
        $this->email->subject(sprintf($this->lang->line('auth_subject_' . $type), $this->config->item('website_name', 'tank_auth')));
        $this->email->message($this->load->view('email/' . $type . '-html', $data, TRUE));
        $this->email->set_alt_message($this->load->view('email/' . $type . '-txt', $data, TRUE));
        $this->email->send();

    }


    /**
     * Create reCAPTCHA JS and non-JS HTML to verify user as a human
     *
     * @return    string
     */
    function _create_recaptcha()
    {
        $this->load->helper('recaptcha');

        // load from CI library
        $this->load->library('recaptcha');


        $data = array(
            'widget' => $this->recaptcha->getWidget(),
            'script' => $this->recaptcha->getScriptTag(),
        );


        return $data;
    }

    /**
     * Callback function. Check if reCAPTCHA test is passed.
     *
     * @return    bool
     */
    function _check_recaptcha()
    {

        $this->load->library('recaptcha');

        $recaptcha = $this->input->post('g-recaptcha-response');

        if (!empty($recaptcha)) {
            $response = $this->recaptcha->verifyResponse($recaptcha);
            if (isset($response['success']) and $response['success'] === true) {
                return TRUE;
            }
        }

        RETURN FALSE;

    }


}