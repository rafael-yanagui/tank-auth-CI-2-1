<div id="pagina-login">

    <div id="conteudo-pagina-login" class="pesquisa-jurisprudencia-topo">

        <div class="container">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1">
                    <hgroup>
                        <h1 class="titulo text-center">Faça o Login para continuar</h1>
                        <h2 class="subtitulo text-center">Caso ainda não possua um cadastro, <a href="<?php echo base_url('cadastro'); ?>" title="Faça seu cadastro no Buscador Dizer o Direito">clique aqui</a></h2>

                    </hgroup>
                </div>

            </div>

            <div class="row">

                <div id="login-usuario" class="login-usuario">

                    <div class="campos-login col-xs-12 col-sm-6 col-md-4 col-xs-offset-0 col-sm-offset-3 col-md-offset-4">
                        <?php
                        if (validation_errors() != '') {
                            echo validation_errors('<div class="panel panel-danger"><div class="panel-heading"><span class="fa fa-exclamation-circle"></span> &nbsp;', '</div></div>');
                        }
                        ?>
                    </div>

                    <div class="campos-login col-xs-12 col-sm-6 col-md-4 col-xs-offset-0 col-sm-offset-3 col-md-offset-4">

                        <div class="acesso-redes-sociais">

                            <div class="chamada-acesso-facebook">

                                <a href="<?php echo base_url('login/auth_oa2/session/facebook') ?>" target="blank" class="btn-acesso-facebook btn btn-default btn-full"> <span class="icone-facebook"></span> Acessar com o Facebook</a>

                            </div>

                        </div>

                        <div class="txt-alternativa-login row">ou</div>

                        <?php echo form_open(base_url($this->uri->uri_string()), array('class' => 'nicely')); ?>

                        <div class="form-group">
                            <label for="">CPF</label>
                            <?php echo form_input('login', set_value('login'), 'id="login" class="input-login form-control" placeholder="CPF" data-mask="000.000.000-00"'); ?>
                        </div>

                        <div class="form-group">
                            <label for="">Senha</label>
                            <?php echo form_password('password', set_value('password'), 'class="input-login form-control" placeholder="Senha"'); ?>
                        </div>

                        <?php if (isset($recaptcha)): ?>
                            <div class="pull-right">

                                <?php echo $recaptcha['widget']; ?>
                                <?php echo $recaptcha['script']; ?>
                            </div>
                            <br/>
                            <div class="clearfix"></div>
                            <br/>
                        <?php endif; ?>

                        <div class="form-group">
                            <input type="submit" class="btn-efetuar-login btn btn-primary btn-full" value="ENTRAR">

                            <div class="checkbox remember-password pull-right">
                                <label>
                                    <?php echo form_checkbox(array(
                                        'name' => 'remember',
                                        'id' => 'remember',
                                        'value' => 1,
                                        'checked' => set_value('remember'),
                                    )); ?> Permanecer conectado.
                                </label>
                                <br/>
                                <br/>
                            </div>
                        </div>


                        <div class="row links-complemento-login">
                            <div class="col-xs-12 col-sm-7 col-md-6"><a href="<?php echo base_url('login/esqueci_a_senha')?>" title="Recupere sua senha aqui."> <em> <u> Esqueci minha senha </u> </em> </a></div>

                            <div class="link-cadastro col-xs-12 col-sm-5 col-md-6"><a href="<?php echo base_url('login/cadastro'); ?>" title="Faça seu cadastro no Buscador Dizer o Direito"> Cadastre-se </a></div>
                        </div>

                        <?php echo form_close(); ?>

                    </div>

                    <div class="clearfix"></div>
                </div>
                <!-- #cadastro-usuario-->

            </div>

        </div>

        <div class="clearfix"></div>

    </div>
    <!-- #conteudo-pagina-login -->

    <div class="clearfix"></div>

</div>
<!-- #pagina-login -->