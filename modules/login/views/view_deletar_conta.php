<div id="pagina-login">

    <div id="conteudo-pagina-login" class="pesquisa-jurisprudencia-topo">

        <div class="container">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1">
                    <hgroup>
                        <h1 class="titulo text-center">Excluir conta</h1>
                        <h2 class="subtitulo text-center">Essa ação é irreversivel, você tem certeza disso?</h2>
                        <p class="subtitulo text-center">Digite sua senha no campo abaixo caso você tenha certeza que deseja excluir sua conta.</p>

                    </hgroup>
                </div>

            </div>

            <div class="row">
                <div id="login-usuario" class="login-usuario">

                    <div class="campos-login col-xs-12 col-sm-6 col-md-4 col-xs-offset-0 col-sm-offset-3 col-md-offset-4">
                        <?php
                        if (validation_errors() != '') {
                            echo validation_errors('<div class="panel panel-danger"><div class="panel-heading"><span class="fa fa-exclamation-circle"></span> &nbsp;', '</div></div>');
                        }
                        ?>
                    </div>

                    <div class="campos-login col-xs-12 col-sm-6 col-md-4 col-xs-offset-0 col-sm-offset-3 col-md-offset-4">

                        <?php echo form_open(base_url($this->uri->uri_string()), array('class' => 'nicely')); ?>

                        <div class="form-group">
                            <label for="">Nova senha</label>
                            <?php if (form_error('password') != '') { ?>
                                &nbsp; <span class="label label-danger">INVÁLIDO!</span>
                            <?php } ?>
                            <?php echo form_password(array(
                                'name' => 'password',
                                'id' => 'password',
                                'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
                                'size' => 30,
                                'class' => 'input-cadastro form-control',
                                'placeholder' => 'Digite sua nova senha'
                            )); ?>
                            <?php if (form_error('password') != '') { ?>
                                <small class="help-block"><?php echo form_error('password') ?></small>
                            <?php } ?>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn-efetuar-login btn btn-primary btn-full" value="Excluir conta">
                        </div>

                        <?php echo form_close(); ?>

                    </div>

                    <div class="clearfix"></div>
                </div>
                <!-- #cadastro-usuario-->

            </div>

        </div>

        <div class="clearfix"></div>

    </div>
    <!-- #conteudo-pagina-login -->

    <div class="clearfix"></div>

</div>
<!-- #pagina-login -->