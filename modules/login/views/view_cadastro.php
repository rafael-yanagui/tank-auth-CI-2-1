<div id="pagina-cadastro">

    <div id="conteudo-pagina-cadastro" class="pesquisa-jurisprudencia-topo">

        <div class="container">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1">
                    <hgroup>
                        <h1 class="titulo text-center">Cadastro</h1>
                        <h2 class="subtitulo text-center">
                            Para ter acesso ao Buscador Dizer o Direito, é necessário ser cadastrado. <br>
                            Por favor, preencha as informações abaixo para prosseguir.
                        </h2>
                    </hgroup>
                </div>

            </div>

            <div class="row">

                <div id="cadastro-usuario" class="cadastro-usuario">

                    <div class="col-xs-12 co-sm-12 col-md-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1">

                        <div class="campos-cadastro col-xs-12 col-sm-6 col-md-6">

                            <?php echo form_open(base_url($this->uri->uri_string()), array('class' => 'nicely')); ?>

                            <div class="form-group ">
                                <label for="">Nome</label>
                                <?php if (form_error('firstname') != '') { ?>
                                    &nbsp; <span class="label label-danger">INVÁLIDO!</span>
                                <?php } ?>

                                <?php echo form_input(array(
                                    'name' => 'firstname',
                                    'value' => set_value('firstname'),
                                    'maxlength' => $this->config->item('firstname_max_length', 'tank_auth'),
                                    'class' => 'form-control',
                                    'placeholder' => 'Nome Completo'
                                )); ?>
                                <?php if (form_error('firstname') != '') { ?>
                                    <small class="help-block"><?php echo form_error('firstname') ?></small>
                                <?php } ?>

                            </div>

                            <div class="form-group">
                                <label for="">Email</label>
                                <?php if (form_error('email') != '') { ?>
                                    &nbsp; <span class="label label-danger">INVÁLIDO!</span>
                                <?php } ?>
                                <?php echo form_input(array(
                                    'type' => 'email',
                                    'name' => 'email',
                                    'value' => set_value('email'),
                                    'maxlength' => 80,
                                    'class' => 'input-cadastro form-control',
                                    'placeholder' => 'Email'
                                )); ?>
                                <?php if (form_error('email') != '') { ?>
                                    <small class="help-block"><?php echo form_error('email') ?></small>
                                <?php } ?>

                            </div>

                            <div class="form-group">
                                <label for="">CPF</label>
                                <?php if (form_error('username') != '') { ?>
                                    &nbsp; <span class="label label-danger">INVÁLIDO!</span>
                                <?php } ?>
                                <?php echo form_input(array(
                                    'name' => 'username',
                                    'value' => set_value('username'),
                                    'maxlength' => 80,
                                    'class' => 'input-cadastro form-control',
                                    'data-mask' => '000.000.000-00',
                                    'placeholder' => 'CPF'
                                )); ?>
                                <?php if (form_error('username') != '') { ?>
                                    <small class="help-block"><?php echo form_error('username') ?></small>
                                <?php } ?>

                            </div>

                            <div class="form-group">
                                <label for="">Senha</label>
                                <?php if (form_error('password') != '') { ?>
                                    &nbsp; <span class="label label-danger">INVÁLIDO!</span>
                                <?php } ?>
                                <?php echo form_password(array(
                                    'name' => 'password',
                                    'value' => set_value('password'),
                                    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
                                    'class' => 'input-cadastro form-control',
                                    'placeholder' => 'Senha'
                                )); ?>
                                <?php if (form_error('password') != '') { ?>
                                    <small class="help-block"><?php echo form_error('password') ?></small>
                                <?php } ?>
                            </div>


                            <div class="form-group">
                                <label for="">Confirme sua senha</label>
                                <?php if (form_error('confirm_password') != '') { ?>
                                    &nbsp; <span class="label label-danger">INVÁLIDO!</span>
                                <?php } ?>
                                <?php echo form_password(array(
                                    'name' => 'confirm_password',
                                    'id' => 'confirm_password',
                                    'value' => set_value('confirm_password'),
                                    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
                                    'class' => 'input-cadastro form-control',
                                    'placeholder' => 'Confirme sua senha'
                                )); ?>
                                <?php if (form_error('confirm_password') != '') { ?>
                                    <small class="help-block"><?php echo form_error('confirm_password') ?></small>
                                <?php } ?>
                            </div>

                            <?php if (isset($recaptcha_html)): ?>
                                <div class="pull-right">
                                    <?php if (form_error('g-recaptcha-response') != '') { ?>
                                        <small class="help-block"><span class="label label-danger">Atenção</span><?php echo form_error('g-recaptcha-response') ?></small>
                                    <?php } ?>
                                    <?php echo $recaptcha_html['widget']; ?>
                                    <?php echo $recaptcha_html['script']; ?>

                                </div>

                                <div class="clearfix"></div>
                            <?php endif; ?>

                            <div class="form-group">
                                <input type="submit" class="btn-efetuar-cadastro btn btn-primary btn-full" value="PROSSEGUIR">
                                <!-- <a href="--><?php //echo base_url('cadastro/completar_cadastro') ?><!--" class="btn-efetuar-cadastro btn btn-primary btn-full">PROSSEGUIR</a>-->
                            </div>

                            <?php form_close(); ?>

                        </div>


                        <div class="acesso-redes-sociais col-xs-12 col-sm-6 col-md-6">

                            <div class="chamada-acesso-facebook">

                                <div class="descricao"> Se preferir, acesse usando seu <br> <strong>Facebook</strong>.</div>

                                <a href="<?php echo base_url('login/auth_oa2/session/facebook') ?>" target="blank" class="btn-acesso-facebook btn btn-default"> <span class="icone-facebook"></span> Cadastrar com o Facebook</a>

                            </div>

                        </div>

                    </div>

                    <div class="clearfix"></div>
                </div>
                <!-- #cadastro-usuario-->

            </div>

        </div>

        <div class="clearfix"></div>

    </div>
    <!-- #conteudo-pagina-cadastro -->

    <div class="clearfix"></div>

</div>
<!-- #pagina-cadastro -->