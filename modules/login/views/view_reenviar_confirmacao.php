<div id="pagina-login">

    <div id="conteudo-pagina-login" class="pesquisa-jurisprudencia-topo">

        <div class="container">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1">
                    <hgroup>
                        <h1 class="titulo text-center">Ative sua conta!</h1>
                        <h2 class="subtitulo text-center">Antes de fazer login, você precisa ativar a sua conta.</h2>

                    </hgroup>
                </div>

            </div>

            <div class="row">
                <div id="login-usuario" class="login-usuario">

                    <div class="campos-login col-xs-12 col-sm-6 col-md-4 col-xs-offset-0 col-sm-offset-3 col-md-offset-4">
                        <?php
                        if (validation_errors() != '') {
                            echo validation_errors('<div class="panel panel-danger"><div class="panel-heading"><span class="fa fa-exclamation-circle"></span> &nbsp;', '</div></div>');
                        }
                        ?>
                    </div>


                    <div class="campos-login col-xs-12 col-sm-6 col-md-4 col-xs-offset-0 col-sm-offset-3 col-md-offset-4">


                        <div class="txt-alternativa-login row">Informe seu e-mail abaixo e receba o e-mail de ativação</div>

                        <?php echo form_open(base_url($this->uri->uri_string()), array('class' => 'nicely')); ?>

                        <div class="form-group">
                            <label for="">Email</label>
                            <?php if (form_error('email') != '') { ?>
                                &nbsp; <span class="label label-danger">INVÁLIDO!</span>
                            <?php } ?>
                            <?php echo form_input(array(
                                'type' => 'email',
                                'name' => 'email',
                                'value' => set_value('email'),
                                'maxlength' => 80,
                                'class' => 'input-cadastro form-control',
                                'placeholder' => 'Email'
                            )); ?>
                            <?php if (form_error('email') != '') { ?>
                                <small class="help-block"><?php echo form_error('email') ?></small>
                            <?php } ?>

                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn-efetuar-login btn btn-primary btn-full" value="Enviar e-mail de ativação">
                        </div>

                        <?php echo form_close(); ?>

                    </div>

                    <div class="clearfix"></div>
                </div>
                <!-- #cadastro-usuario-->

            </div>

        </div>

        <div class="clearfix"></div>

    </div>
    <!-- #conteudo-pagina-login -->

    <div class="clearfix"></div>

</div>
<!-- #pagina-login -->