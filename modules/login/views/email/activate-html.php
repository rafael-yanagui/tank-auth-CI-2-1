<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Bem vindo ao <?php echo $site_name; ?>!</title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
    <table width="80%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="5%"></td>
            <td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
                <h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;">Bem vindo ao <?php echo $site_name; ?>!</h2>
                Obrigado por juntar-se ao <?php echo $site_name; ?>. Este e-mail contém dados de sua conta, mantenha esse e-mail seguro e não compartilhe com ninguém.<br/>
                Para ativar seu e-mail, acesse o link abaixo:<br/>
                <br/>
                <big style="font: 16px/18px Arial, Helvetica, sans-serif;"><b><a href="<?php echo base_url('/login/ativacao/' . $user_id . '/' . $new_email_key); ?>" style="color: #3366cc;">Clique aqui para ativar sua conta...</a></b></big><br/>
                <br/>
                O link não funciona? Copie o link  na barra de endereço de seu navegador:<br/>
                <nobr><a href="<?php echo base_url('/login/ativacao/' . $user_id . '/' . $new_email_key); ?>" style="color: #3366cc;"><?php echo base_url('/login/ativacao/' . $user_id . '/' . $new_email_key); ?></a></nobr>
                <br/>
                <br/>
                Por favor ative sua conta dentro de <?php echo $activation_period; ?> horas, se o tempo ultrapassar esse limite seu cadastro se tornará inválido e você precisará se registrar novamente.<br/>
                <br/>
                <br/>
                <?php if (strlen($username) > 0) { ?>Seu Login: <strong><?php echo $username; ?></strong><br/><?php } ?>
                Seu e-mail: <strong><?php echo $email; ?></strong><br/>
                <?php if (isset($password)) { /* ?>Your password: <?php echo $password; ?><br /><?php */
                } ?>
                <br/>
                <br/>
                Atenciosamente!<br/>
                Equipe <?php echo $site_name; ?>
            </td>
        </tr>
    </table>
</div>
</body>
</html>