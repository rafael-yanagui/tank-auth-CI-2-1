Bem vindo ao <?php echo $site_name; ?>,

Obrigado por juntar-se ao <?php echo $site_name; ?>.  Este e-mail contém dados de sua conta, mantenha esse e-mail seguro e não compartilhe com ninguém.
Para ativar seu e-mail, acesse o link abaixo:

<?php echo base_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?>


Por favor ative sua conta dentro de <?php echo $activation_period; ?> horas, se o tempo ultrapassar esse limite seu cadastro se tornará inválido e você precisará se registrar novamente.
<?php if (strlen($username) > 0) { ?>

Seu login: <?php echo $username; ?>
<?php } ?>

Seu e-mail: <?php echo $email; ?>
<?php if (isset($password)) { /* ?>

Your password: <?php echo $password; ?>
<?php */ } ?>



Atenciosamente,
Equipe <?php echo $site_name; ?>