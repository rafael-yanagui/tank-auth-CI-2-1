<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Alterar senha em <?php echo $site_name; ?></title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
    <table width="80%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="5%"></td>
            <td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
                <h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;">Alterar senha</h2>
                Esqueceu sua senha? Não se preocupe.<br/>
                Para criar uma nova senha, acesse o link abaixo:<br/>
                <br/>
                <big style="font: 16px/18px Arial, Helvetica, sans-serif;"><b><a href="<?php echo base_url('/login/alterar_senha/' . $user_id . '/' . $new_pass_key); ?>" style="color: #3366cc;">Criar uma nova senha</a></b></big><br/>
                <br/>
                O link acima não funciona? Copie o link abaixo em seu navegador:<br/>
                <nobr><a href="<?php echo base_url('/login/alterar_senha/' . $user_id . '/' . $new_pass_key); ?>" style="color: #3366cc;"><?php echo base_url('/login/alterar_senha/' . $user_id . '/' . $new_pass_key); ?></a></nobr>
                <br/>
                <br/>
                <br/>
                Você este e-mail porque você soliciou uma nova senha no site <a href="<?php echo base_url(''); ?>" style="color: #3366cc;"><?php echo $site_name; ?></a>. Se você NÃO SOLICITOU uma nova senha, apenas ignore este e-mail.
                <br/>
                <br/>
                <br/>
                Atenciosamente,<br/>
                Equipe <?php echo $site_name; ?>
            </td>
        </tr>
    </table>
</div>
</body>
</html>