<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Welcome to <?php echo $site_name; ?>!</title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
    <table width="80%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="5%"></td>
            <td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
                <h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;">Bem vindo ao <?php echo $site_name; ?>!</h2>
                Obrigado por se juntar ao <?php echo $site_name; ?>. Veja os detalhes de seu cadastro abaixo, não compartilhe este e-mail com ninguém.<br/>
                Para abrir o <?php echo $site_name; ?> , por favor siga o link abaixo:<br/>
                <br/>
                <big style="font: 16px/18px Arial, Helvetica, sans-serif;"><b><a href="<?php echo site_url('login/'); ?>" style="color: #3366cc;">Ir para <?php echo $site_name; ?> agora!</a></b></big><br/>
                <br/>
                O Link não funciona? Copie o link abaixo em seu navegado:<br/>
                <nobr><a href="<?php echo base_url('login/'); ?>" style="color: #3366cc;"><?php echo base_url('login/'); ?></a></nobr>
                <br/>
                <br/>
                <br/>
                <?php if (isset($username) && strlen($username) > 0) { ?>Seu usuário: <?php echo $username; ?><br/><?php } ?>
                Seu e-mail: <?php echo $email; ?><br/>
                <?php /* Your password: <?php echo $password; ?><br /> */ ?>
                <br/>
                <br/>
                Divirta-se!<br/>

            </td>
        </tr>
    </table>
</div>
</body>
</html>