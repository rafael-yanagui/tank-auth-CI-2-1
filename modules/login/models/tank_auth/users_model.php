<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users
 *
 * This model represents user authentication data. It operates the following tables:
 * - user account data,
 * - user profiles
 *
 * @package    Tank_auth
 * @author    Ilya Konyukhov (http://konyukhov.com/soft/)
 */
class Users_model extends CI_Model
{
    private $tableUser;
    private $tableUserProfile;

    private $table_name = 'users';            // user accounts
    private $profile_table_name = 'user_profiles';    // user profiles

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();

        $this->tableUser = $ci->config->item('db_table_user', 'tank_auth');
        $this->tableUserProfile = $ci->config->item('db_table_user_profile', 'tank_auth');

        $this->table_name = $this->tableUser['tableName'];
        $this->profile_table_name = $this->tableUserProfile['tableName'];
    }

    /**
     * Get user record by Id
     *
     * @param    int
     * @param    bool
     * @return    object
     */
    function get_user_by_id($user_id, $activated)
    {
        $this->db->where($this->tableUser['id'], $user_id);
        $this->db->where($this->tableUser['activated'], $activated ? 1 : 0);

        $query = $this->db->get($this->table_name);
        if ($query->num_rows() == 1) return prepareDbReturn($query->row_array(), $this->tableUser);;
        return NULL;
    }

    /**
     * Get user record by login (username or email)
     *
     * @param    string
     * @return    object
     */
    function get_user_by_login($login, $provider = 'site')
    {
        $this->db->where('LOWER(' . $this->tableUser['username'] . ')=', strtolower($login));
        $this->db->or_where('LOWER(' . $this->tableUser['email'] . ')=', strtolower($login));

        $this->db->where('provider', $provider);


        $query = $this->db->get($this->table_name);
        if ($query->num_rows() == 1) return prepareDbReturn($query->row_array(), $this->tableUser);;
        return NULL;
    }

    /**
     * Get user record by username
     *
     * @param    string
     * @return    object
     */
    function get_user_by_username($username)
    {
        $this->db->where('LOWER(' . $this->tableUser['username'] . ')=', strtolower($username));

        $query = $this->db->get($this->table_name);
        if ($query->num_rows() == 1) return prepareDbReturn($query->row_array(), $this->tableUser);;
        return NULL;
    }

    /**
     * Get user record by email
     *
     * @param    string
     * @return    object
     */
    function get_user_by_email($email, $provider = 'site')
    {
        $this->db->where('LOWER(' . $this->tableUser['email'] . ')=', strtolower($email));

        $this->db->where($this->tableUser['provider'], $provider);

        $query = $this->db->get($this->table_name);
        if ($query->num_rows() == 1) return prepareDbReturn($query->row_array(), $this->tableUser);;
        return NULL;
    }

    /**
     * Check if username available for registering
     *
     * @param    string
     * @return    bool
     */
    function is_username_available($username)
    {
        $this->db->select('1', FALSE);
        $this->db->where('LOWER(' . $this->tableUser['username'] . ')=', strtolower($username));

        $query = $this->db->get($this->table_name);
        return $query->num_rows() == 0;
    }

    /**
     * Check if email available for registering
     *
     * @param    string
     * @return    bool
     */
    function is_email_available($email)
    {
        $this->db->select('1', FALSE);
        $this->db->where('LOWER(' . $this->tableUser['email'] . ')=', strtolower($email));
        $this->db->or_where('LOWER(' . $this->tableUser['new_email'] . ')=', strtolower($email));

        $query = $this->db->get($this->table_name);
        return $query->num_rows() == 0;
    }

    /**
     * Create new user record
     *
     * @param    array
     * @param    bool
     * @return    array
     */
    function create_user($data, $activated = TRUE, $require_complete_profile = TRUE)
    {
        $user = array();

        foreach ($data as $key => $value) { // Converto os nomes das colunas conforme o arquivo de configuração
            if (isset($this->tableUser[$key])) {
                $user[$this->tableUser[$key]] = $value;
            } else {
                $user[$key] = $value;
            }
        }

        $user[$this->tableUser['created']] = date('Y-m-d H:i:s');

        $user[$this->tableUser['activated']] = $activated ? 1 : 0;

        $user[$this->tableUser['completed_profile']] = $require_complete_profile ? 0 : 1;

        if ($this->db->insert($this->table_name, $user)) {
            $user_id = $this->db->insert_id();
            if ($activated) $this->create_profile($user_id);
            return array('user_id' => $user_id);
        }
        return NULL;
    }

    /**
     * Activate user if activation key is valid.
     * Can be called for not activated users only.
     *
     * @param    int
     * @param    string
     * @param    bool
     * @return    bool
     */
    function activate_user($user_id, $activation_key, $activate_by_email)
    {
        $this->db->select('1', FALSE);
        $this->db->where($this->tableUser['id'], $user_id);
        if ($activate_by_email) {
            $this->db->where($this->tableUser['new_email_key'], $activation_key);
        } else {
            $this->db->where($this->tableUser['new_password_key'], $activation_key);
        }
        $this->db->where($this->tableUser['activated'], 0);
        $query = $this->db->get($this->table_name);

        if ($query->num_rows() == 1) {

            $this->db->set($this->tableUser['activated'], 1);
            $this->db->set($this->tableUser['new_email_key'], NULL);
            $this->db->where($this->tableUser['id'], $user_id);
            $this->db->update($this->table_name);

            $this->create_profile($user_id);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Purge table of non-activated users
     *
     * @param    int
     * @return    void
     */
    function purge_na($expire_period = 172800)
    {
        $this->db->where($this->tableUser['activated'], 0);
        $this->db->where('UNIX_TIMESTAMP(' . $this->tableUser['created'] . ') <', time() - $expire_period);
        $this->db->delete($this->table_name);
    }

    /**
     * Delete user record
     *
     * @param    int
     * @return    bool
     */
    function delete_user($user_id)
    {
        $this->db->where($this->tableUser['id'], $user_id);
        $this->db->delete($this->table_name);
        if ($this->db->affected_rows() > 0) {
            $this->delete_profile($user_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Set new password key for user.
     * This key can be used for authentication when resetting user's password.
     *
     * @param    int
     * @param    string
     * @return    bool
     */
    function set_password_key($user_id, $new_pass_key)
    {
        $this->db->set($this->tableUser['new_password_key'], $new_pass_key);
        $this->db->set($this->tableUser['new_password_requested'], date('Y-m-d H:i:s'));
        $this->db->where($this->tableUser['id'], $user_id);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Check if given password key is valid and user is authenticated.
     *
     * @param    int
     * @param    string
     * @param    int
     * @return    void
     */
    function can_reset_password($user_id, $new_pass_key, $expire_period = 900)
    {
        $this->db->select('1', FALSE);
        $this->db->where($this->tableUser['id'], $user_id);
        $this->db->where($this->tableUser['new_password_key'], $new_pass_key);
        $this->db->where('UNIX_TIMESTAMP(' . $this->tableUser['new_password_requested'] . ') >', time() - $expire_period);

        $query = $this->db->get($this->table_name);
        return $query->num_rows() == 1;
    }

    /**
     * Change user password if password key is valid and user is authenticated.
     *
     * @param    int
     * @param    string
     * @param    string
     * @param    int
     * @return    bool
     */
    function reset_password($user_id, $new_pass, $new_pass_key, $expire_period = 900)
    {
        $this->db->set($this->tableUser['password'], $new_pass);
        $this->db->set($this->tableUser['new_password_key'], NULL);
        $this->db->set($this->tableUser['new_password_requested'], NULL);
        $this->db->where($this->tableUser['id'], $user_id);
        $this->db->where($this->tableUser['new_password_key'], $new_pass_key);
        $this->db->where('UNIX_TIMESTAMP(' . $this->tableUser['new_password_requested'] . ') >=', time() - $expire_period);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Change user password
     *
     * @param    int
     * @param    string
     * @return    bool
     */
    function change_password($user_id, $new_pass)
    {
        $this->db->set($this->tableUser['password'], $new_pass);
        $this->db->where($this->tableUser['id'], $user_id);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Set new email for user (may be activated or not).
     * The new email cannot be used for login or notification before it is activated.
     *
     * @param    int
     * @param    string
     * @param    string
     * @param    bool
     * @return    bool
     */
    function set_new_email($user_id, $new_email, $new_email_key, $activated)
    {
        $this->db->set($activated ? $this->tableUser['new_email'] : $this->tableUser['email'], $new_email);
        $this->db->set($this->tableUser['new_email_key'], $new_email_key);
        $this->db->where($this->tableUser['id'], $user_id);
        $this->db->where($this->tableUser['activated'], $activated ? 1 : 0);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Activate new email (replace old email with new one) if activation key is valid.
     *
     * @param    int
     * @param    string
     * @return    bool
     */
    function activate_new_email($user_id, $new_email_key)
    {
        $this->db->set($this->tableUser['email'], 'new_email', FALSE);
        $this->db->set($this->tableUser['new_email'], NULL);
        $this->db->set($this->tableUser['new_email_key'], NULL);
        $this->db->where($this->tableUser['id'], $user_id);
        $this->db->where($this->tableUser['new_email_key'], $new_email_key);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Update user login info, such as IP-address or login time, and
     * clear previously generated (but not activated) passwords.
     *
     * @param    int
     * @param    bool
     * @param    bool
     * @return    void
     */
    function update_login_info($user_id, $record_ip, $record_time)
    {
        $this->db->set($this->tableUser['new_password_key'], NULL);
        $this->db->set($this->tableUser['new_password_requested'], NULL);

        if ($record_ip) $this->db->set($this->tableUser['last_ip'], $this->input->ip_address());
        if ($record_time) $this->db->set($this->tableUser['last_login'], date('Y-m-d H:i:s'));

        $this->db->where($this->tableUser['id'], $user_id);
        $this->db->update($this->table_name);
    }

    /**
     * Ban user
     *
     * @param    int
     * @param    string
     * @return    void
     */
    function ban_user($user_id, $reason = NULL)
    {
        $this->db->where($this->tableUser['id'], $user_id);
        $this->db->update($this->table_name, array(
            $this->tableUser['banned'] => 1,
            $this->tableUser['ban_reason'] => $reason,
        ));
    }

    /**
     * Unban user
     *
     * @param    int
     * @return    void
     */
    function unban_user($user_id)
    {
        $this->db->where($this->tableUser['id'], $user_id);
        $this->db->update($this->table_name, array(
            $this->tableUser['banned'] => 0,
            $this->tableUser['ban_reason'] => NULL,
        ));
    }

    /**
     * Create an empty profile for a new user
     *
     * @param    int
     * @return    bool
     */
    private function create_profile($user_id)
    {
        $this->db->set($this->tableUserProfile['id'], $user_id);
        return $this->db->insert($this->profile_table_name);
    }

    /**
     * Delete user profile
     *
     * @param    int
     * @return    void
     */
    private function delete_profile($user_id)
    {
        $this->db->where($this->tableUserProfile['id'], $user_id);
        $this->db->delete($this->profile_table_name);
    }
}

/* End of file users.php */
/* Location: ./application/models/auth/users.php */