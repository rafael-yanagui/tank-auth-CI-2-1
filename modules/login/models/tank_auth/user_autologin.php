<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User_Autologin
 *
 * This model represents user autologin data. It can be used
 * for user verification when user claims his autologin passport.
 *
 * @package    Tank_auth
 * @author    Ilya Konyukhov (http://konyukhov.com/soft/)
 */
class User_Autologin extends CI_Model
{

    private $tableUserAutologin;
    private $tableUser;

    private $table_name;
    private $users_table_name;

    function __construct()
    {
        parent::__construct();

        $ci =& get_instance();

        $this->tableUserAutologin = $ci->config->item('db_table_user_autologin', 'tank_auth');
        $this->tableUser = $ci->config->item('db_table_user', 'tank_auth');

        $this->table_name = $this->tableUserAutologin['tableName'];
        $this->users_table_name = $this->tableUser['tableName'];
    }

    /**
     * Get user data for auto-logged in user.
     * Return NULL if given key or user ID is invalid.
     *
     * @param    int
     * @param    string
     * @return    object
     */
    function get($user_id, $key)
    {
        $this->db->select($this->users_table_name . '.' . $this->tableUser['id']);
        $this->db->select($this->users_table_name . '.' . $this->tableUser['username']);
        $this->db->select($this->users_table_name . '.' . $this->tableUser['completed_profile']);
        $this->db->from($this->users_table_name);
        $this->db->join($this->table_name, $this->table_name . '.' . $this->tableUserAutologin['user_id'] . ' = ' . $this->users_table_name . '.' . $this->tableUser['id']);
        $this->db->where($this->table_name . '.' . $this->tableUserAutologin['user_id'], $user_id);
        $this->db->where($this->table_name . '.' . $this->tableUserAutologin['key_id'], $key);
        $query = $this->db->get();
        if ($query->num_rows() == 1) return prepareDbReturn($query->row_array(), $this->tableUser);

        return NULL;
    }

    /**
     * Save data for user's autologin
     *
     * @param    int
     * @param    string
     * @return    bool
     */
    function set($user_id, $key)
    {
        return $this->db->insert($this->table_name, array(
            $this->tableUserAutologin['user_id'] => $user_id,
            $this->tableUserAutologin['key_id'] => $key,
            $this->tableUserAutologin['user_agent'] => substr($this->input->user_agent(), 0, 149),
            $this->tableUserAutologin['last_ip'] => $this->input->ip_address(),
        ));
    }

    /**
     * Delete user's autologin data
     *
     * @param    int
     * @param    string
     * @return    void
     */
    function delete($user_id, $key)
    {
        $this->db->where($this->tableUserAutologin['user_id'], $user_id);
        $this->db->where($this->tableUserAutologin['key_id'], $key);
        $this->db->delete($this->table_name);
    }

    /**
     * Delete all autologin data for given user
     *
     * @param    int
     * @return    void
     */
    function clear($user_id)
    {
        $this->db->where($this->tableUserAutologin['user_id'], $user_id);
        $this->db->delete($this->table_name);
    }

    /**
     * Purge autologin data for given user and login conditions
     *
     * @param    int
     * @return    void
     */
    function purge($user_id)
    {
        $this->db->where($this->tableUserAutologin['user_id'], $user_id);
        $this->db->where($this->tableUserAutologin['user_agent'], substr($this->input->user_agent(), 0, 149));
        $this->db->where($this->tableUserAutologin['last_ip'], $this->input->ip_address());
        $this->db->delete($this->table_name);
    }
}

/* End of file user_autologin.php */
/* Location: ./application/models/auth/user_autologin.php */