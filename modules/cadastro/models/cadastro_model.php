<?php

/**
 * Created by PhpStorm.
 * User: Rafael
 * Date: 09/09/2016
 * Time: 16:43
 */
class cadastro_model extends CI_Model
{

    public function getAllPosicaoDireito()
    {
        $this->db->select('*');
        $this->db->where('deletado', '0');
        $this->db->where('bloqueado', '0');
        $query = $this->db->get('posicao_direito');
        if (!$query) {
            log_message('error', 'DB - Erro ao listar as posicoes de direito - ' . $this->db->_error_message());
            return false;
        }

        return $query->result();
    }

    public function atualizar($data)
    {
        $data['data_edicao'] = date('Y-m-d H:i:s');

        $this->db->where('id', $data['id']);

        if ($this->db->update('usuario', $data)) {
            $this->completarPerfil($data['id']);
            return TRUE;
        }
        return FALSE;
    }

    public function completarPerfil($idLogin)
    {
        $this->session->set_userdata('completed_profile', TRUE);

        $this->db->where('id', $idLogin);
        $this->db->set('perfil_atualizado', '1');
        $this->db->update('login');
    }
}