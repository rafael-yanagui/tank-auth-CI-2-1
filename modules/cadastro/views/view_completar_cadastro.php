<div id="pagina-cadastro" class="pagina-completar-cadastro">

    <div id="conteudo-pagina-cadastro" class="pesquisa-jurisprudencia-topo">

        <div class="container">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1">
                    <hgroup>
                        <h1 class="titulo text-center">Falta só mais um pouco...</h1>
                        <h2 class="subtitulo text-center">Para termos uma interação mais personalizada, por favor insira as informações abaixo.</h2>
                    </hgroup>
                </div>

            </div>

            <div class="row">

                <div id="cadastro-usuario" class="cadastro-usuario completar-cadastro-usuario">

                    <div class="campos-login col-xs-12 col-sm-6 col-md-4 col-xs-offset-0 col-sm-offset-3 col-md-offset-4">
                        <?php
                        if (validation_errors() != '') {
                            echo validation_errors('<div class="panel panel-danger"><div class="panel-heading"><span class="fa fa-exclamation-circle"></span> &nbsp;', '</div></div>');
                        }
                        ?>
                    </div>

                    <div class="campos-cadastro col-xs-12 col-sm-6 col-md-4 col-xs-offset-0 col-sm-offset-3 col-md-offset-4">

                        <?php echo form_open(base_url($this->uri->uri_string()), array('class' => 'nicely')); ?>


                        <div class="form-group">
                            <label for="">Sexo</label>
                            <?php if (form_error('sexo') != '') { ?>
                                &nbsp; <span class="label label-danger">INVÁLIDO!</span>
                            <?php }
                            echo form_dropdown('sexo', array('' => 'Selecione o seu sexo', 'masculino' => 'Masculino', 'feminino' => 'Feminino'), set_value('sexo'), 'class="selectpicker"');
                            if (form_error('sexo') != '') { ?>
                                <small class="help-block"><?php echo form_error('sexo') ?></small>
                            <?php } ?>
                        </div>

                        <div class="form-group">
                            <label for="">Cidade</label>
                            <?php if (form_error('cidade') != '') { ?>
                                &nbsp; <span class="label label-danger">INVÁLIDO!</span>
                            <?php }
                            echo form_input('cidade', set_value('cidade'), 'class="input-cadastro form-control" placeholder="Digite o nome da sua cidade"');
                            if (form_error('cidade') != '') { ?>
                                <small class="help-block"><?php echo form_error('cidade') ?></small>
                            <?php } ?>
                        </div>

                        <div class="form-group">
                            <label for="">UF</label>
                            <?php if (form_error('uf') != '') { ?>
                                &nbsp; <span class="label label-danger">INVÁLIDO!</span>
                            <?php }
                            echo form_dropdown('uf', listarUf(), set_value('uf'), 'class="selectpicker"');
                            if (form_error('username') != '') { ?>
                                <small class="uf-block"><?php echo form_error('uf') ?></small>
                            <?php } ?>
                        </div>

                        <div class="form-group">
                            <label for="">Posição no Direito</label>
                            <?php if (form_error('posicao') != '') { ?>
                                &nbsp; <span class="label label-danger">INVÁLIDO!</span>
                            <?php }
                            echo form_dropdown('posicao', $posicoesDireito, set_value('posicao'), 'class="selectpicker"');
                            if (form_error('posicao') != '') { ?>
                                <small class="help-block"><?php echo form_error('posicao') ?></small>
                            <?php } ?>

                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn-efetuar-cadastro btn btn-primary btn-full" value="COMPLETAR CADASTRO E COMEÇAR">
                        </div>

                        <?php echo form_close(); ?>


                    </div>

                    <div class="clearfix"></div>
                </div>
                <!-- #cadastro-usuario-->

            </div>

        </div>

        <div class="clearfix"></div>

    </div>
    <!-- #conteudo-pagina-cadastro -->

    <div class="clearfix"></div>

</div>
<!-- #pagina-cadastro -->