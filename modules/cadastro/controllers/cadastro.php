<?php
/**
 * Created by PhpStorm.
 * User: maurilio
 * Date: 25/07/16
 * Time: 17:38
 */

if (!defined('BASEPATH')) {
    exit('Acesso negado!');
}

class Cadastro extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("cadastro/cadastro_model");
    }


    public function completar_cadastro()
    {

        // Valido os campos do formulário
        $this->form_validation->set_rules('sexo', 'Sexo', 'trim|required|xss_clean');
        $this->form_validation->set_rules('cidade', 'Cidade', 'trim|required|xss_clean');
        $this->form_validation->set_rules('uf', 'UF', 'trim|required|xss_clean');
        $this->form_validation->set_rules('posicao', 'Posicao', 'trim|required|xss_clean');


        $data['errors'] = array();
        if ($this->form_validation->run()) { // validation ok

            //Faço o tratamento do nome para cadastrar nome separado de sobrenome
            $usuario['id'] = $this->session->userdata('login_id');
            $usuario['sexo'] = $this->input->post('sexo');
            $usuario['cidade'] = $this->input->post('sexo');
            $usuario['uf'] = $this->input->post('sexo');
            $usuario['posicao_direito_id'] = $this->input->post('posicao');

            // Cadastro o usuário
            if ($this->cadastro_model->atualizar($usuario)) {// success

                $this->_show_message("Parabéns, seu cadastro agora está completo.", 'success');

            } else {

                $errors = $this->tank_auth->get_error_message();
                foreach ($errors as $k => $v) {
                    $this->form_validation->set_error($k, $this->lang->line($v));
                }
            }
        }


        $posicoesDireito = $this->cadastro_model->getAllPosicaoDireito();
        $vetorCombo[''] = 'Selecione sua posição no direito';
        foreach ($posicoesDireito as $posicao) {
            $vetorCombo[$posicao->id] = $posicao->nome;
        }
        $data['posicoesDireito'] = $vetorCombo;

        $data['title'] = "Complete seu cadastro e tenha acesso ao Buscador Dizer o Direito";
        $data['description'] = "Complete seu cadastro e tenha acesso ao Buscador Dizer o Direito";
        $data['menu_ativo'] = "completar_cadastro";

        $this->template->show('cadastro/view_completar_cadastro', $data);
    }

    private function _show_message($message, $type = 'default')
    {
        if ($type == 'danger')
            $this->session->set_flashdata('alert_controller_danger', $message);
        else if ($type == 'warning')
            $this->session->set_flashdata('alert_controller_warning', $message);
        else if ($type == 'success')
            $this->session->set_flashdata('alert_controller_success', $message);
        else
            $this->session->set_flashdata('msg_controller', $message);


        redirect(base_url());

    }


}