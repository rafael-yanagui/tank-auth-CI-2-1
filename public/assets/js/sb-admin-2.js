$(function () {

    $('#side-menu').metisMenu();

    $('.textarea-html').wysihtml5();

    completaEndereco();

    //$(".inputMoney").maskMoney({showSymbol:true, symbol:"", decimal:",", thousands:"."});

    //    $('.inputMoney').blur(function (){
    //        formataNumero(this);
    //    })
    // efeitoLinks();


    $('[data-toggle="tooltip"]').tooltip();

});


//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function () {

    $(window).bind("load resize", function () {

        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse')
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse')
        }

        height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
        height = height - topOffset;
        if (height < 1)
            height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }

    });

});

/* Alertas customizados */

function alerta(titulo, texto, tipo) {

    if (titulo == null)
        titulo = "Alerta";

    sweetAlert(titulo, texto, tipo);

}


//Mudar de campo com o Enter no login
function enter(event, btn) {
    var key = event.keyCode || event.which;

    if (key == 13)
        $(btn).click();
    else
        return;
}


function exibirModal(url, titulo) {

    // url = pagina que sera aberta no modal
    if (titulo == "") {
        titulo = '';
    }

    $('#myModalContainer .modal-title').text(titulo);
    $('#myModalContainer .modal-body').load(url);

    $('#myModalContainer').modal({
        keyboard: true
    });

}


function removerLinhaTabela(handler) {

    var tr = $(handler).closest('tr');

    tr.fadeOut(400, function () {
        tr.remove();
    });

    return false;

}

// Sempre que estiver editando algo em uma tabela, vai adicionar o alerta antes do .table-responsive
function alertaTabela(msg, tipo) {

    // Removo o alert que pode existir (pra nao empilhar varios)
    $('.alert').remove();

    // Exibe um alert antes da tabela
    $('.table-responsive').before('<div class="alert ' + tipo + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + msg + '</div>');

}

/**
 *
 * @param {type} msg
 * @param {type} tipo
 * @returns {undefined}
 *
 * Tipos: alert-danger alert-success alert-info
 */
function alertaTopoPagina(msg, tipo) {

    // Remove o alert que pode existir (pra nao empilhar varios)
    $('.alert').remove();


    // Exibe um alert abaixo do titulo da pagina
    $('.page-header').after('<div class="alert ' + tipo + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + msg + '</div>');

}


function completaEndereco() {

    /**
     *
     * @param {type} key
     * @returns {String}
     *
     * Campos a serem trabalhados
     * CEP - endereco-cep
     * Rua - endereco-rua
     * Bairro - endereco-bairro
     * UF - endereco-UF
     *
     * E setar campo no numero
     */

    $('.localidade-cep').blur(function () {
        getEndereco($(this).val());
    });
}


function getEndereco(cep) {


    /**
     *
     * @ect id="endereco-uf" name="zone_id">
     <option value=""> --- Selecione --- </option>
     <option value="440">Acre</option>
     <option value="441">Alagoas</option>
     <option value="442">Amapa</option>
     <option value="443">Amazonas</option>
     <option value="444">Bahia</option>
     <option value="445">Ceara</option>
     <option value="446">Distrito Federal</option>
     <option value="447">Espirito Santo</option>
     <option value="448">Goias</option>
     <option value="449">Maranhao</option>
     <option value="450">Mato Grosso</option>
     <option value="451">Mato Grosso do Sul</option>
     <option value="452">Minas Gerais</option>
     <option value="453">Para</option>
     <option value="454">Paraiba</option>
     <option value="455">Parana</option>
     <option value="456">Pernambuco</option>
     <option value="457">Piaui­</option>
     <option value="458">Rio de Janeiro</option>
     <option value="459">Rio Grande do Norte</option>
     <option value="460">Rio Grande do Sul</option>
     <option value="461">Rondonia</option>
     <option value="462">Roraima</option>
     <option value="463">Santa Catarina</option>
     <option value="464">Sao Paulo</option>
     <option value="465">Sergipe</option>
     <option value="466">Tocantins</option>
     */
        // Cria o vetor de todos os Estados pra setar via ajax
    var estados = [];
    estados['AC'] = "440";
    estados['AL'] = "441";
    estados['AM'] = "442";
    estados['AP'] = "443";
    estados['BA'] = "444";
    estados['CE'] = "445";
    estados['DF'] = "446";
    estados['ES'] = "447";
    estados['GO'] = "448";
    estados['MA'] = "449";
    estados['MT'] = "450";
    estados['MS'] = "451";
    estados['MG'] = "452";
    estados['PA'] = "453";
    estados['PB'] = "454";
    estados['PR'] = "455";
    estados['PE'] = "456";
    estados['PI'] = "457";
    estados['RJ'] = "458";
    estados['RN'] = "459";
    estados['RS'] = "460";
    estados['RO'] = "461";
    estados['RR'] = "462";
    estados['SC'] = "463";
    estados['SP'] = "464";
    estados['SE'] = "465";
    estados['TO'] = "466";

    if ($.trim(cep) != "") {


        $(".loading-cep").html(' Aguarde...');
        $.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep=" + cep, function () {
            if (resultadoCEP["resultado"] != 0) {
                $(".localidade-endereco").val(unescape(resultadoCEP["logradouro"]) + ', ');
                $(".localidade-bairro").val(unescape(resultadoCEP["bairro"]));
                $(".localidade-cidade").val(unescape(resultadoCEP["cidade"]));
                $(".localidade-uf").val(estados[unescape(resultadoCEP["uf"])]);

                $(".loading-cep").html('');

                // Tudo correndo bem, exibe um alerta para informar o numero.
                alertaTopoPagina('O endereço foi preenchido. Por favor, informe o número após a vírgula.', 'alert-info');

                $(".loading-cep").html('');
                $(".localidade-endereco").focus();

            } else {
                $(".localidade-endereco").html(unescape(resultadoCEP["resultado_txt"]));
            }
        });

    }
    else {
        $(".loading-cep").html('<span style="font-size: 0.8em; color: #e00;">Informe o CEP</span>');
    }
}


function loading() {
    if ($('#loading-template').is(':hidden')) {
        $('#loading-template').show();
    } else {
        $('#loading-template').hide();
    }
}


function formataNumero(campo) {


    $(campo).formatCurrency({
        symbol: '',
        decimalSymbol: ',',
        digitGroupSymbol: '.'
    });

    var retorno = campo.value;


    if (campo.getAttribute('maxlength') < retorno.length) {
        alert(' Tamanho máximo do campo formatado deve ser igual ou menor que  ' + campo.getAttribute('maxlength'));
        campo.value = '0,00';
    }
}

function efeitoLinks() {
    $('.sidebar-nav a, #page-wrapper .form-filtro a, #page-wrapper .form-filtro button, .nav.nav-tabs li a.link-externo').click(function () {
        loading();
    });
}


function alertaConfirmarSair(msg) {
    this.msg = msg;
    this.needToConfirm = false;

    var myself = this;

    window.onbeforeunload = function () {
        if (myself.needToConfirm) {
            return myself.msg;
        }
    }
}
// Para instanciar este objeto
// var exitPage = new alertaConfirmarSair("Minha mensagem de confirmação.");


function ativarValidacaoForm() {

    $('input').change(function () {
        ativarConfirmacao = true;
    });
    $('form').submit(function () {
        ativarConfirmacao = false;
    });

}


function carregarComboDinamico(urlController, cbPai, cbFilho, idCbFilho) {

    idCbPai = cbPai.val();

    if (idCbPai === '')
        return false;

    resetarCombo(cbFilho);

    $.getJSON(base_url + urlController + '/' + idCbPai, function (data) {

        var option = new Array();

        $.each(data, function (i, obj) {

            option[i] = document.createElement('option');
            $(option[i]).attr({value: obj.id});
            $(option[i]).append(obj.nome);

            cbFilho.append(option[i]);

        });

        // Se for edicao, vai selecionar o item correto
        if (idCbFilho != '')
            cbFilho.val(idCbFilho);

    });

}

/**
 *
 * @param {type} urlController
 * @param {type} cbPai
 * @param {type} cbFilho
 * @param {type} idCbFilho
 * @returns {undefined}
 */
function gerenciarEventosComboDinamico(urlController, cbPai, cbFilho, idCbFilho) {

    // Se for na edicao, o combo da categoria estara selecionado, e assim carrega o combo de Subcategorias
    if (cbPai.val() != "") {
        carregarComboDinamico(urlController, cbPai, cbFilho, idCbFilho);
    }

    // Sempre que selecionar um Categoria, carrega novamente o combo de Subcategorias
    cbPai.change(function () {
        carregarComboDinamico(urlController, cbPai, cbFilho);
    });
}


function resetarCombo(el) {
    $(el).empty();
    var option = document.createElement('option');
    $(option).attr({value: ''});
    $(option).append('Selecione');
    el.append(option);
}