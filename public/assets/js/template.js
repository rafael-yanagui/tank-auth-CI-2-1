/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    containerFull = $('#wrapper');
    containerLoading = $("#page-loader");

    containerFull.hide();

    // Chama funcao quando carrega a pagina ou quando redimensiona a tela
    preLoad();
    $(window).load(function () {
        preLoad();
    });  

    $('.selectpicker').selectpicker();


    $('#sidebar-menu-categorias li').each(function(index, element) {
        if($(this).find('ul').length > 0) {
            console.log($(this).children('a').append("<span class='caret'></span>"));
        }
    });

    /**
     * Evento que oculta ou mostra as opções de pesquisa no topo
     * Os nomes das classes dos botoes sao os mesmos, o que difere é o nome do elemento pai
     */

    $('#pagina-detalhes-julgado .btn-chamada-filtro-topo, #pagina-download .btn-chamada-filtro-topo').click(function() {
        $('.pesquisa-jurisprudencia-topo').slideDown();
        $(this).hide();
        $('.btn-fechar-filtro-topo').show();
    });

    $('#pagina-detalhes-julgado .btn-fechar-filtro-topo, #pagina-download .btn-fechar-filtro-topo').click(function() {
        $('.pesquisa-jurisprudencia-topo').slideUp();
        $(this).hide();
        $('.btn-chamada-filtro-topo').show();
    });


    $(function () {
        $('#pesquisa-jurisprudencia-data-1').datetimepicker({
            locale: 'pt-br',
            format: 'DD/MM/YYYY',
            showTodayButton: true,
            showClose: true,
            focusOnShow: false,
            ignoreReadonly: true,
            useCurrent: false, //Important! See issue #1075
            allowInputToggle: true
        });

        $('#pesquisa-jurisprudencia-data-2').datetimepicker({
            locale: 'pt-br',
            format: 'DD/MM/YYYY',
            showTodayButton: true,
            showClose: true,
            focusOnShow: false,
            ignoreReadonly: true,
            useCurrent: false, //Important! See issue #1075
            allowInputToggle: true
        });

        $("#pesquisa-jurisprudencia-data-1").on("dp.change", function (e) {
            $('#pesquisa-jurisprudencia-data-2').data("DateTimePicker").minDate(e.date);
        });

        $("#pesquisa-jurisprudencia-data-2").on("dp.change", function (e) {
            $('#pesquisa-jurisprudencia-data-1').data("DateTimePicker").maxDate(e.date);
        });
    });
  

});

function preLoad() {

    alturaTela = $(window).height();
    larguraTela = $(window).width();

    containerLoading.css("width", larguraTela);
    containerLoading.css("height", alturaTela);

    containerLoading.show();

}

window.onload = function () {
    containerFull.fadeIn();
    containerLoading.fadeOut();
};

