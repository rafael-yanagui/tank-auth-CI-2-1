<?php

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = '6LeGtRYTAAAAAAXk0qYaxVDxJCA2A2xgDf8qf2Tw';
$config['recaptcha_secret_key'] = '6LeGtRYTAAAAAL3hnL3cIxuouw49vlBrjS_-cRk7';

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'pt';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
