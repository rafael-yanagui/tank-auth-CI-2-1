<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Website details
|
| These details are used in emails sent by authentication library.
|--------------------------------------------------------------------------
*/
$config['website_name'] = 'Dizer o Direito';
$config['webmaster_email'] = 'contato@dizerodireito.com.br';

/*
|--------------------------------------------------------------------------
| Security settings
|
| The library uses PasswordHash library for operating with hashed passwords.
| 'phpass_hash_portable' = Can passwords be dumped and exported to another server. If set to FALSE then you won't be able to use this database on another server.
| 'phpass_hash_strength' = Password hash strength.
|--------------------------------------------------------------------------
*/
$config['phpass_hash_portable'] = TRUE;
$config['phpass_hash_strength'] = 8;

/*
|--------------------------------------------------------------------------
| Registration settings
|
| 'allow_registration' = Registration is enabled or not
| 'captcha_registration' = Registration uses CAPTCHA
| 'email_activation' = Requires user to activate their account using email after registration.
| 'email_activation_expire' = Time before users who don't activate their account getting deleted from database. Default is 48 hours (60*60*24*2).
| 'email_account_details' = Email with account details is sent after registration (only when 'email_activation' is FALSE).
| 'require_complete_profile' = Ao Cadastrar, o usuário é redirecionado para uma pagina para concluir seu cadastro.
| 'controller_name_complete_profile' = Nome da classe que está a view de completar cadastro.
| 'use_username' = Username is required or not.
|
| 'username_min_length' = Min length of user's username.
| 'username_max_length' = Max length of user's username.
| 'password_min_length' = Min length of user's password.
| 'password_max_length' = Max length of user's password.
|--------------------------------------------------------------------------
*/
$config['allow_registration'] = TRUE;
$config['recaptcha_registration'] = FALSE;
$config['email_activation'] = FALSE;
$config['email_activation_expire'] = 60 * 60 * 24 * 2;
$config['email_account_details'] = TRUE;

$config['require_complete_profile'] = TRUE;
$config['controller_name_complete_profile'] = 'cadastro';
$config['function_name_complete_profile'] = 'completar_cadastro';

$config['use_username'] = TRUE;

$config['username_min_length'] = 4;
$config['username_max_length'] = 20;
$config['password_min_length'] = 4;
$config['password_max_length'] = 20;

$config['firstname_min_length'] = 2;
$config['firstname_max_length'] = 40;
$config['lastname_min_length'] = 2;
$config['lastname_max_length'] = 40;


/*
|--------------------------------------------------------------------------
| Login settings
|
| 'login_by_username' = Username can be used to login.
| 'login_by_email' = Email can be used to login.
| You have to set at least one of 2 settings above to TRUE.
| 'login_by_username' makes sense only when 'use_username' is TRUE.
|
| 'login_record_ip' = Save in database user IP address on user login.
| 'login_record_time' = Save in database current time on user login.
|
| 'login_count_attempts' = Count failed login attempts.
| 'login_max_attempts' = Number of failed login attempts before CAPTCHA will be shown.
| 'login_attempt_expire' = Time to live for every attempt to login. Default is 24 hours (60*60*24).
|--------------------------------------------------------------------------
*/
$config['login_by_username'] = TRUE;
$config['login_by_email'] = FALSE;
$config['login_record_ip'] = TRUE;
$config['login_record_time'] = TRUE;
$config['login_count_attempts'] = TRUE;
$config['login_max_attempts'] = 5;
$config['login_attempt_expire'] = 60 * 60 * 24;

/*
|--------------------------------------------------------------------------
| Auto login settings
|
| 'autologin_cookie_name' = Auto login cookie name.
| 'autologin_cookie_life' = Auto login cookie life before expired. Default is 2 months (60*60*24*31*2).
|--------------------------------------------------------------------------
*/
$config['autologin_cookie_name'] = 'autologin';
$config['autologin_cookie_life'] = 60 * 60 * 24 * 31 * 2;

/*
|--------------------------------------------------------------------------
| Forgot password settings
|
| 'forgot_password_expire' = Time before forgot password key become invalid. Default is 15 minutes (60*15).
|--------------------------------------------------------------------------
*/
$config['forgot_password_expire'] = 60 * 60 * 3; // 3 hours

/*
|--------------------------------------------------------------------------
| reCAPTCHA
|
| 'use_recaptcha' = Use reCAPTCHA instead of common captcha
| You can get reCAPTCHA keys by registering at http://recaptcha.net
|--------------------------------------------------------------------------
*/
$config['use_recaptcha'] = TRUE;
$config['recaptcha_public_key'] = '6Lcn6xkTAAAAAJvAl0dO9ohbNNXFKimGFFg2Divg';
$config['recaptcha_private_key'] = '6Lcn6xkTAAAAAAQ0gsxkOvHw-GFL78UTNjbMb3I2';

/*
|--------------------------------------------------------------------------
| Database settings
|
| 'db_table_prefix' = Table prefix that will be prepended to every table name used by the library
| (except 'ci_sessions' table).
|--------------------------------------------------------------------------
*/
$config['db_table_prefix'] = '';


$config['db_table_user'] = array(
    'tableName' => $config['db_table_prefix'] . 'login',

    'id' => 'id',
    'username' => 'cpf',
    'password' => 'senha',
    'email' => 'email',
    'provider' => 'provider',
    'firstname' => 'nome',
    'lastname' => 'sobrenome',
    'activated' => 'ativado',
    'completed_profile' => 'perfil_atualizado',
    'banned' => 'bloqueado',
    'ban_reason' => 'razao_bloqueio',
    'new_password_key' => 'chave_nova_senha',
    'new_password_requested' => 'data_requisicao_senha',
    'new_email' => 'novo_email',
    'new_email_key' => 'chave_novo_email',
    'last_ip' => 'ultimo_ip',
    'last_login' => 'data_ultimo_login',
    'created' => 'data_criacao',
    'modified' => 'data_edicao',
);

$config['db_table_user_autologin'] = array(
    'tableName' => $config['db_table_prefix'] . 'usuario_autologin',

    'key_id' => 'key_id',
    'user_id' => 'login_id',
    'user_agent' => 'user_agent',
    'last_ip' => 'ultimo_ip',
    'last_login' => 'ultimo_acesso',

);
$config['db_table_login_attempts'] = array(
    'tableName' => $config['db_table_prefix'] . 'tentativas_login_usuario',

    'key_id' => 'key_id',
    'ip_address' => 'endereco_ip',
    'login' => 'login',
    'time' => 'data_tentativa',

);
$config['db_table_user_profile'] = array(
    'tableName' => 'usuario', // table perfil do usuário
    'id' => 'id',
);



/* End of file tank_auth.php */
/* Location: ./application/config/tank_auth.php */