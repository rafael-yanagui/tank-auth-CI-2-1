<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('prepareDbReturn')) {

    function prepareDbReturn($row, $configTable)
    {
        $coluns = array_flip($configTable);

        foreach ($row as $key => $value) {
            if (isset($coluns[$key])) {
                $returnRow[$coluns[$key]] = $value;
            } else {
                $returnRow[$key] = $value;
            }
        }
        return (object)$returnRow;

    }

}