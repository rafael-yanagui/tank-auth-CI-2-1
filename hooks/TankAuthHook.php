<?php

/**
 * Created by PhpStorm.
 * User: Rafael
 * Date: 09/09/2016
 * Time: 15:26
 */
class TankAuthHook
{

    public $ci;
    // Valida se o usuário já completou o cadastro de seu perfil
    /**
     * TankAuthHook constructor.
     */
    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function validateUser()
    {
        if ($this->ci->session->userdata('completed_profile') !== FALSE) {
            if ($this->ci->session->userdata('completed_profile') === (string)'0' AND $this->ci->config->item('require_complete_profile', 'tank_auth') == TRUE) {

                if ($this->ci->router->fetch_class() != $this->ci->config->item('controller_name_complete_profile', 'tank_auth') && $this->ci->router->fetch_class() != 'login') {
                    redirect(
                        base_url(
                            $this->ci->config->item('controller_name_complete_profile', 'tank_auth') . "/" . ($this->ci->config->item('function_name_complete_profile', 'tank_auth') == 'index' ? '' : $this->ci->config->item('function_name_complete_profile', 'tank_auth'))
                        )
                    );
                }
            }
        }
    }
}